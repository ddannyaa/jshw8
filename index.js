//1
let paragraphs = document.querySelectorAll("p");
paragraphs.forEach((paragraph) => {
    paragraph.style.backgroundColor = '#ff0000';
  });

//2
let initialElement = document.getElementById("optionsList");
console.log(initialElement);
//Знайти батьківський елемент та вивести в консоль
let parent = initialElement.parentNode;
console.log(parent);
// Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод
if (initialElement && initialElement.childNodes.length > 0) {
    initialElement.childNodes.forEach((childNode) => {
      console.log('name: ' + childNode.nodeName + ' type: ' + childNode.nodeType);
    });
  }
//3
let element = document.getElementById('testParagraph');
element.innerText = "This is a paragraph";
console.log(element);
//4
let mainHeader  = document.querySelector('.main-header');
if (mainHeader) {
    let nestedElements = mainHeader.querySelectorAll('*');
    nestedElements.forEach(function(element) {
      element.classList.add('nav-item');
    });
}
console.log(mainHeader);
//5
const elementSectionTitle = document.querySelectorAll('.section-title');
elementSectionTitle.forEach((element) => {
    element.classList.remove('section-title');
  });
  
console.log(elementSectionTitle)
